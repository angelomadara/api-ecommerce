<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Products::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'details' => $faker->paragraph,
        'price' => $faker->randomFloat(2,100,1000),
        'stock' => $faker->randomDigit,
        'discount' => $faker->numberBetween(1,50),
    ];
});
