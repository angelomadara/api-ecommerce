<?php

use Faker\Generator as Faker;
use App\Models\Products;

$factory->define(App\Models\Reviews::class, function (Faker $faker) {
    return [
        'product_id' => function(){return Products::all()->random();},
        'customer' => $faker->name,
        'review' => $faker->paragraph,
        'star' => $faker->numberBetween(0,5),
    ];
});
